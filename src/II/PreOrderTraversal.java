package II;
import java.util.ArrayList;
import java.util.List;

public class PreOrderTraversal implements Traversal
{
	private List<Node> listNode = new ArrayList<Node>();
	
	@Override
	public List<Node> traverse(Node node) 
	{   
		if(node != null)
		{
			listNode.add(node);
			traverse(node.getLeft());
			traverse(node.getRight());
		}
		return listNode;
	}

}
