package II;

public class ReportConsole
{
	public void display(Node root, Traversal traversal)
	{
		if(traversal.getClass() == PreOrderTraversal.class){
			System.out.print("Traverse with PreOrder: ");
		}
		else if(traversal.getClass() == InOrderTraversal.class){
			System.out.print("Traverse with InOrder: ");
		}
		else{
			System.out.print("Traverse with PostOder: ");
		}
		for(Node node : traversal.traverse(root) )
		{
			System.out.print(node.getValue()+" ");
		}
		System.out.println("");

	}
}
