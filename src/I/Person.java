package I;

public class Person implements Comparable<Person>,Taxable{
	String name;
	double Income;
	public Person (String n,double income){
		name= n;
		Income = income;
	}
	public String getName(){
		return name;
	}
	public double getIncome(){
		return Income;
	}
	@Override
	public int compareTo(Person o) {
		// TODO Auto-generated method stub
		return (int) (this.Income-o.getIncome());
	}
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double sum=0;
		if(Income>300000){
			double tax1 = (300000*5)/100;
			double tax2 = ((Income-300000)*10)/100;
			sum = tax1+tax2;
			}
		else{
			sum = (Income*5)/100;
		}
		return sum;

	}
	
}
