package I;

public class Product implements Comparable<Product>,Taxable{
	String name;
	double Price;
	public Product (String n,double price){
		name= n;
		Price = price;
	}
	public String getName(){
		return name;
	}
	public double getPrice(){
		return Price;
	}
	@Override
	public int compareTo(Product o) {
		// TODO Auto-generated method stub
		return (int) (this.Price-o.getPrice());
	}
	@Override
	public double getTax() {
		double ans = (Price*7)/100;
		return ans;
	}

	
}
