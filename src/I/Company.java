package I;

public class Company implements Taxable{
	 String name;
	 double earn;
	 double expense;
	public Company (String n ,double ea,double ex){
		name = n;
		earn = ea;
		expense = ex;
	}
	public String getName(){
		return name;
	}
	public double getEarn(){
		return earn;
	}
	public double getExpense(){
		return expense;
	}
	public double getPro(){
		return earn-expense;
	}
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double val = earn-expense;
		double ans = (val*30)/100;
		return ans;

	}
}
