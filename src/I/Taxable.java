package I;

public interface Taxable {
	String getName();
	double getTax();
}
