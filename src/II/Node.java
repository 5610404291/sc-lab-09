package II;

public class Node
{
	private Node leftNode;
	private Node rightNode;
	private String value;
	
	public Node(String value)
	{
		this.value = value;
	}
	
	public Node getLeft()
	{
		return leftNode;
	}
	
	public Node getRight()
	{
		return rightNode;
	}
	
	public String getValue()
	{
		return value;
	}
	
	public void setLeft(Node leftNode)
	{
		this.leftNode = leftNode;
	}
	
	public void setRight(Node rightNode)
	{
		this.rightNode = rightNode;
	}
}
