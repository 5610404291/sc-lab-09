package I;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class tescase {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Person> per = new ArrayList<Person>();
		List<Company> com = new ArrayList<Company>();
		List<Product> pro = new ArrayList<Product>();
		List<Taxable> tax = new ArrayList<Taxable>();
		
		Person p1 =new Person("p1",20000);
		Person p2 =new Person("p2",30000);
		Person p3 =new Person("p3",50000);
		System.out.print("Compare ");
		if (p1.compareTo(p2) > 0) {
			System.out.println(p1.getName()+" is over than "+p2.getName()+" "+p1.compareTo(p2));
		}
		else if (p1.compareTo(p2) < 0) {
			System.out.println(p1.getName()+" is less than "+p2.getName()+" "+(int)Math.abs(p1.compareTo(p2)));
		}
		
		if (p3.compareTo(p2) > 0) {
			System.out.println(p3.getName()+" is over than "+p2.getName()+" "+p3.compareTo(p2));
		}
		else if (p3.compareTo(p2) < 0) {
			System.out.println(p3.getName()+" is less than "+p2.getName()+" "+(int)Math.abs(p3.compareTo(p2)));
		}
		per.add(p1);per.add(p2);per.add(p3);
		Collections.sort(per);
		System.out.println("Sort by Value");

		for (int i =0;i < per.size();i++) {
			System.out.println(per.get(i).getName()+" : "+per.get(i).getIncome());
		}

		Product pro1 = new Product("Pro1" ,1000);
		Product pro2 = new Product("Pro2" ,2000);
		Product pro3 = new Product("Pro3" ,3000);
		System.out.println("Compare : ");
		if (pro1.compareTo(pro2) > 0) {
			System.out.println(pro1.getName()+" is over than "+pro2.getName()+" "+pro1.compareTo(pro2));
		}
		else if (pro1.compareTo(pro2) < 0) {
			System.out.println(pro1.getName()+" is less than "+pro2.getName()+" "+(int)Math.abs(pro1.compareTo(pro2)));
		}
		
		if (pro3.compareTo(pro2) > 0) {
			System.out.println(pro3.getName()+" is over than "+pro2.getName()+" "+pro3.compareTo(pro2));
		}
		else if (pro3.compareTo(pro2) < 0) {
			System.out.println(pro3.getName()+" is less than "+pro2.getName()+" "+(int)Math.abs(pro3.compareTo(pro2)));
		}
		pro.add(pro1);pro.add(pro2);pro.add(pro3);
		
		System.out.println("Sort by Value");
		Collections.sort(pro);
		for (int i =0;i < pro.size();i++) {
			System.out.println(pro.get(i).getName()+" : "+pro.get(i).getPrice());
		}
		Company c = new Company("C1", 600000,200000);
		Company c2 = new Company("C2", 800000,700000);
		Company c3 = new Company("C3", 500000,100000);
		
		EarningComparator enc = new EarningComparator();
		ExpenseComparator exc = new ExpenseComparator();
		ProfitComparator procom = new ProfitComparator();
		System.out.println("Earning compare : ");
		//Earning
		if (enc.compare(c, c2) > 0) {
			System.out.println(c.getName()+ " earning over than "+c2.getName()+" "+enc.compare(c, c2));
		}
		else if (enc.compare(c, c2) < 0) {
			System.out.println(c.getName()+ " earning less than "+c2.getName()+" "+(int)Math.abs(enc.compare(c, c2)));
		}
		if (enc.compare(c, c3) > 0) {
			System.out.println(c.getName()+ " earning over than "+c3.getName()+" "+enc.compare(c, c3));
		}
		else if (enc.compare(c, c3) < 0) {
			System.out.println(c.getName()+ " earning less than "+c3.getName()+" "+(int)Math.abs(enc.compare(c, c3)));
		}
		System.out.println("---------------------------");
		System.out.println("Expense compare : ");
		//Expense
		if (exc.compare(c, c2) > 0) {
			System.out.println(c.getName()+ " expense over than "+c2.getName()+" "+exc.compare(c, c2));
		}
		else if (exc.compare(c, c2) < 0) {
			System.out.println(c.getName()+ " expense less than "+c2.getName()+" "+(int)Math.abs(exc.compare(c, c2)));
		}
		if (exc.compare(c, c3) > 0) {
			System.out.println(c.getName()+ " expense over than "+c3.getName()+" "+exc.compare(c, c3));
		}
		else if (exc.compare(c, c3) < 0) {
			System.out.println(c.getName()+ " expense less than "+c3.getName()+" "+(int)Math.abs(exc.compare(c, c3)));
		}
		System.out.println("---------------------------");
		System.out.println("Profit compare : ");
		//Profit
		if (procom.compare(c, c2) > 0) {
			System.out.println(c.getName()+ " profit over than "+c2.getName()+" "+procom.compare(c, c2));
		}
		else if (procom.compare(c, c2) < 0) {
			System.out.println(c.getName()+ " profit less than "+c2.getName()+" "+(int)Math.abs(procom.compare(c, c2)));
		}
		if (procom.compare(c, c3) > 0) {
			System.out.println(c.getName()+ " profit over than "+c3.getName()+" "+procom.compare(c, c3));
		}
		else if (procom.compare(c, c3) < 0) {
			System.out.println(c.getName()+ " profit less than "+c3.getName()+" "+(int)Math.abs(procom.compare(c, c3)));
		}
		System.out.println("---------------------------");
	
		com.add(c);
		com.add(c2);
		com.add(c3);
		System.out.println("Sort by Earning");
		Collections.sort(com,enc);
		for (int i =0;i < com.size();i++) {
			System.out.println(com.get(i).getName()+" : "+com.get(i).getEarn());
		}
		System.out.println("---------------------------");
		System.out.println("Sort by Expense");
		Collections.sort(com,exc);
		for (int i =0;i < com.size();i++) {
			System.out.println(com.get(i).getName()+" : "+com.get(i).getExpense());
		}
		System.out.println("---------------------------");
		System.out.println("Sort by Profit");
		Collections.sort(com,procom);
		for (int i =0;i < com.size();i++) {
			System.out.println(com.get(i).getName()+" : "+com.get(i).getPro());
		}

		tax.add(p1);
		tax.add(p2);
		tax.add(p3);
		tax.add(c);
		tax.add(c2);
		tax.add(c3);
		tax.add(pro1);
		tax.add(pro3);
		tax.add(pro2);
		TaxableComparator txc = new TaxableComparator();
		Collections.sort(tax,txc);
		for (int i =0;i < tax.size();i++) {
			System.out.println((tax.get(i)).getName()+" : "+tax.get(i).getTax());
		}

	}

}
