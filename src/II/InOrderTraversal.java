package II;
import java.util.ArrayList;
import java.util.List;

public class InOrderTraversal implements Traversal
{
	private List<Node> listNode = new ArrayList<Node>();

	@Override
	public List<Node> traverse(Node node) 
	{
		if(node != null)
		{
			traverse(node.getLeft());
			listNode.add(node);
			traverse(node.getRight());
		}
		return listNode;
	}



}
