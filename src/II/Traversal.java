package II;
import java.util.List;


public interface Traversal 
{
	List<Node> traverse(Node node);
}
