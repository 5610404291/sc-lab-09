package II;

public class TraverseTest {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Node node_A = new Node("A");
		Node node_B = new Node("B");
		Node node_C = new Node("C");
		Node node_D = new Node("D");
		Node node_E = new Node("E");
		Node node_F = new Node("F");
		Node node_G = new Node("G");
		Node node_H = new Node("H");
		Node node_I = new Node("I");

		node_F.setLeft(node_B);
		node_F.setRight(node_G);

		node_B.setLeft(node_A);
		node_B.setRight(node_D);

		node_D.setLeft(node_C);
		node_D.setRight(node_E);

		node_G.setRight(node_I);

		node_I.setLeft(node_H);
		
		PreOrderTraversal preOrder = new PreOrderTraversal();
		InOrderTraversal inOrder = new InOrderTraversal();
		PostOrderTraversal postOrder = new PostOrderTraversal();
		
		ReportConsole console = new ReportConsole();
		
		console.display(node_F, preOrder);
		console.display(node_F, inOrder);
		console.display(node_F, postOrder);
		
		
	}

}
