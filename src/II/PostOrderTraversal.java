package II;
import java.util.ArrayList;
import java.util.List;

public class PostOrderTraversal implements Traversal
{
	private List<Node> listNode = new ArrayList<Node>();

	@Override
	public List<Node> traverse(Node node) 
	{
		if(node != null)
		{
			traverse(node.getLeft());
			traverse(node.getRight());
			listNode.add(node);
		}
		return listNode;
	}

}
